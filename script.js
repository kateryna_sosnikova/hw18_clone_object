let obj = {
    a: 'name',
    b: 'surname',
    c: {
        d: 134,
        e: 'student',
        f: {
            g: 'value',
            e: false,
        }
    },
    d: [1, 2, 3],
};

function copyObjFunc(obj) {
    let copyObj = {};
    for (let key in obj) {
        if (Array.isArray(obj[key])) {
            copyObj[key] = obj[key].slice(0);
        }
        else if (typeof obj[key] == 'object') {
            copyObj[key] = copyObjFunc(obj[key]);
        }
        else {
            copyObj[key] = obj[key];
        }
    }
    return copyObj;
}

console.log(copyObjFunc(obj));
